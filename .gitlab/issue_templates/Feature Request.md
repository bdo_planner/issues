<!---
Before posted any new issues, please check the README which details common 
feature requests https://gitlab.com/bdo_planner/issues/blob/master/README.md
as well as our Roadmap https://trello.com/b/XGV1cvtb/bdo-planner-roadmap
and check to make sure to check the issue hasn't already been posted 
https://gitlab.com/bdo_planner/issues/issues

Thanks
BDO Planner Team
-->

**Description**  
<!--- Please describe the request here. What part of the UI do you want changed? -->
<!--- How do you want it to look/work in comparison to how it is now? -->