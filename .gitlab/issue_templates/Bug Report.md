<!---
Before posted any new issues, please check the README which details common 
issues https://gitlab.com/bdo_planner/issues/blob/master/README.md and check
to make sure to check the issue hasn't already been posted 
https://gitlab.com/bdo_planner/issues/issues

Thanks
BDO Planner Team
-->

**Description**  
<!--- Please give a short description of the issue here -->


**Steps to Reproduce**  
<!--- Provide all the steps necessary to reproduce the problem -->
1. Step 1
2. Step 2
3. Step 3
4. Step 4


**Expected Behavior**  
<!--- Tell us what should happen -->


**Actual Behavior**  
<!--- Tell us what happens instead -->