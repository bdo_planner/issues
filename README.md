# BDO Planner - Issues and Feature Requests

Please read this before posting any issues. This will be updated with common Issues and Feature requests that we will not be looking handle.

## Common Issues

-   None

## Common Feature Requests

-   Multi-language support  
    _Coming soon._  
    Related Issues: [#46](https://gitlab.com/bdo_planner/issues/issues/46) [#59](https://gitlab.com/bdo_planner/issues/issues/59)

## Issue labels

| Category | Label |
| ------ | ------ |
| Type | ~"type::bug" ~"type::feature" ~"type::idea" ~"type::general" |
| Area | ~"area::api" ~"area::app" ~"area::cdn" ~"area::data" ~"area::devsite" ~"area::server" |
| Feedback | ~"feedback::discussion" |
| Priority | ~"priority::low" ~"priority::medium" ~"priority::high" |
| Inactive | ~"inactive::invalid" ~"inactive::wont fix" ~"inactive::duplicate" ~"inactive::on hold" |